package com.grolinger.java;

import com.grolinger.java.config.PlantumlStubberConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@EnableConfigurationProperties(value = {PlantumlStubberConfiguration.class})
@SpringBootApplication(scanBasePackages = {"com.grolinger.java"})
public class PlantumlStubber {
    private static String port;

    @Value("${server.port:19191}")
    public void setPort(String port) {
        PlantumlStubber.port = port;
    }

    public static void main(String[] args) {
        SpringApplication.run(PlantumlStubber.class, args);
        log.info("GUI accessible via http://localhost:{}/swagger-ui.html#/", port);
    }

}
