package com.grolinger.java.service.data;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import static com.grolinger.java.controller.templatemodel.Constants.DIR_UP;

public interface CommonRootPathHandler {
    /**
     * "Calculates" the path to the root directory to be able to descent into the common directory.
     * All generated iuml files load the common.iuml file that contains a number of commonly used
     * !functions and !procedures and also loads participants or components depending on the type of
     * diagram
     *
     * @return {@code ../} default | a number of {@code ../} depending on how many slashes are in the service's name
     * the last part for interfaces should end without slash e.g. like this {@code ../../..}
     *
     */
    default String getPathToRoot() {
        // the default value is one directory up (../) because of the application's directory itself
        if (null == getNameParts())
            return DIR_UP.getValue();
        else {
            StringBuilder cp = new StringBuilder();
            int numberOfTraversedDirectories = getNameParts().size();
            // The last part of the interface is a file, and therefore we need to subtract
            // 1 from number of directories to go up in the hierarchy
            if (isInterface()) numberOfTraversedDirectories--;
            cp.append(String.valueOf(DIR_UP.getValue())
                    .repeat(Math.max(0, numberOfTraversedDirectories)));

            return isInterface() ? StringUtils.stripEnd(cp.toString(), "/") : cp.toString();
        }
    }

    List<String> getNameParts();

    boolean isInterface();
}
