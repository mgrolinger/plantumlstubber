package com.grolinger.java.service.adapter.exportdata;

import com.grolinger.java.controller.templatemodel.DiagramType;

import java.io.IOException;

public interface LocalStaticAdapter {
    /**
     * This method copies a file to a specific folder within the
     * GLOBAL_FILE_EXPORT_PATH, which is currently hardcoded to
     * target, because I don't need something else.
     *
     * @param folder defines the subfolder below GLOBAL_FILE_EXPORT_PATH, to which the file will be copied
     * @param currentFileName the filename of the file to copy
     */
    void copyFile(final String folder, final String currentFileName);

    /**
     * Exports a template that should be used to specify a service.
     * Then this can be used with this application to generate the diagrams.
     * This feature is useful, when only a jar is distributed, so the
     * user can export the resources/_template_newApplication.yaml
     */
    void exportTemplate();

    /**
     * Writes the important common.iuml file that contains a lot of the common definitions, such as functions or skinparams
     *
     * @param basePath    To where the file(s) will be exported
     * @param diagramType For which diagram type (component/sequence) the specific common.iuml is exported
     */
    void writeDefaultCommonFile(final String basePath, final DiagramType diagramType) throws IOException;

    /**
     * Write files from the Security folder
     */
    void writeSecurityFiles() throws IOException;


    /**
     * Writes skin files to the export directory.
     *
     * @throws IOException e.g. if the output directory is not writable
     */
    void writeDefaultSkinFiles() throws IOException;
}
