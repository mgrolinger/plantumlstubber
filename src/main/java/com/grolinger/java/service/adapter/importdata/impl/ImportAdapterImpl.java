package com.grolinger.java.service.adapter.importdata.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.grolinger.java.service.adapter.importdata.ImportAdapter;
import com.grolinger.java.service.data.ApplicationDefinition;
import com.grolinger.java.service.data.InterfaceDefinition;
import com.grolinger.java.service.data.ServiceDefinition;
import com.grolinger.java.service.data.importdata.ImportedServices;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.grolinger.java.service.NameConverter.replaceUnwantedPlantUMLCharacters;

/**
 * This service maps information from the yaml files to internal *definitions.
 */
@Slf4j
@Service
public class ImportAdapterImpl implements ImportAdapter {
    private static final String GLOBAL_FILE_EXPORT_PATH = System.getProperty("user.dir") + File.separator + "target" + File.separator;

    /**
     * Reads the file from the filesystem path and maps it to the internal data model
     *
     * @return map with application_service as key and the definitions as value
     */

    @Override
    public List<ApplicationDefinition> findAllServiceEndpoints() {

        return getFileList()
                // get the list of the files from the file walker
                .stream()
                // map it to the internal data model
                .map(this::mapFilesToDefinitions)
                // unwrap list of lists
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<ApplicationDefinition> mapFilesToDefinitions(Path fileToMap) {
        List<ImportedServices> services = new LinkedList<>(readYamlFilesFromFilesystem(fileToMap));

        Map<String, ApplicationDefinition> app = new HashMap<>();

        for (ImportedServices importedServices : services) {
            if (importedServices == null || importedServices.getServices() == null) {
                continue;
            }

            log.debug("{}, {}, {}", importedServices.getApplication(), importedServices.getSystemType(), importedServices.getOrderPrio());
            ApplicationDefinition pumlComponent = mapToApplicationDefinition(importedServices, app);

            // Save if needed in some other round
            app.put(importedServices.getApplication(), pumlComponent);
        }
        return new LinkedList<>(app.values());
    }

    private ApplicationDefinition mapToApplicationDefinition(ImportedServices importedServices, Map<String, ApplicationDefinition> app) {
        // Do we know this application already from before, reuse it.
        if (app.containsKey(importedServices.getApplication())) {
            return app.get(importedServices.getApplication());
        }

        int orderPrio = Integer.parseInt(importedServices.getOrderPrio());
        // we use the specified alias or the application name cleaned from some special characters that make problems in plantuml
        String alias = StringUtils.isEmpty(importedServices.getCustomAlias()) ?
                replaceUnwantedPlantUMLCharacters(importedServices.getApplication().toLowerCase(), false)
                        .replaceAll("_", "") :
                importedServices.getCustomAlias();
        // we use the specified label or the application name cleaned from some special characters that make problems in plantuml
        String label = StringUtils.isEmpty(importedServices.getCustomLabel()) ?
                replaceUnwantedPlantUMLCharacters(importedServices.getApplication(), false) :
                importedServices.getCustomLabel();

        return ApplicationDefinition.builder()
                .name(importedServices.getApplication())
                .label(label)
                .alias(alias)
                .systemType(importedServices.getSystemType())
                .orderPrio(orderPrio)
                .serviceDefinitions(getServiceDefinitions(importedServices))
                .build();
    }

    /**
     * Generates the service definitions from the imported files
     *
     * @param importedServices definitions from the yaml file
     * @return list of service definitions
     */
    private List<ServiceDefinition> getServiceDefinitions(ImportedServices importedServices) {
        List<ServiceDefinition> result = new LinkedList<>();
        // Iterate over Services.<REST|SOAP|...>
        for (String interfacesIntegrationType : importedServices.getServices().keySet()) {
            Map<String, String[]> serviceList = importedServices.getServices().get(interfacesIntegrationType);
            // Iterate over the services itself
            List<ServiceDefinition> serviceDefinitions = mapServiceDefinitions(importedServices, interfacesIntegrationType, serviceList);
            result.addAll(serviceDefinitions);
        }
        return result;
    }

    /**
     * Extracts the services from yaml file and maps them hierarchically to internal structures.
     *
     * @param importedServices          service definitions from the yaml
     * @param interfacesIntegrationType type of integration
     * @param serviceList               list of services
     * @return list of mapped services
     */
    private List<ServiceDefinition> mapServiceDefinitions(ImportedServices importedServices, String interfacesIntegrationType, Map<String, String[]> serviceList) {
        List<ServiceDefinition> serviceDefinitions = new LinkedList<>();
        //serviceName is a key-value pair, consisting of the service (key) and 1..n interfaces (value)
        for (Map.Entry<String, String[]> serviceName : serviceList.entrySet()) {
            String clearServiceName = trimStartAndEndSeparator(serviceName.getKey());
            // services are stored in the map keys
            ServiceDefinition serviceDefinition = ServiceDefinition.builder()
                    .serviceLabel(serviceName.getKey())
                    .serviceName(clearServiceName)
                    .domainColor(importedServices.getDomainColor())
                    .build();
            //Interfaces are stored in the map values
            List<InterfaceDefinition> interfaceDefinitionsList = mapInterfaces(
                    importedServices,
                    interfacesIntegrationType,
                    serviceName.getValue(),
                    importedServices.getDomainColor());
            serviceDefinition.getInterfaceDefinitions().addAll(interfaceDefinitionsList);
            serviceDefinitions.add(serviceDefinition);
        }
        return serviceDefinitions;
    }

    /**
     * The serviceName in the yaml often contains separators that result in the wrong directory structure later
     * Thus, we remove this separators from the string.
     * Example: In the yaml a service may be defined as follows /api/v1/, should result in the directory api and
     * its subdirectory v1 and is also used to calculate the relative path to the root directory. However, the
     * starting / causes problems with the resulting relative path being to long. Instead of being ../../ it is
     * often calculated ../../../../ (for this particular example)
     * @param serviceNameToClean the service name with potential separator chars e.g. /api/v1/
     * @return the cleaned name without starting and ending separator api/v1
     */
    private static String trimStartAndEndSeparator(String serviceNameToClean) {
        List<String> separators = List.of("_", "/");
        for (String separator : separators) {
            serviceNameToClean = StringUtils.stripStart(serviceNameToClean, separator);
            serviceNameToClean = StringUtils.stripEnd(serviceNameToClean, separator);
        }
        return serviceNameToClean;
    }

    /**
     * Extracts the interfaces from the yaml file
     *
     * @param importedServices          Imported services from yaml
     * @param interfacesIntegrationType integration such as REST::JSON or SOAP::XML
     * @param interfaceNames            a number of interface names
     * @param domainColor               application domain color
     * @return list of interfaces
     */
    private List<InterfaceDefinition> mapInterfaces(ImportedServices importedServices, String interfacesIntegrationType, String[] interfaceNames, String domainColor) {
        List<InterfaceDefinition> interfaceDefinitions = new LinkedList<>();
        for (String interfaceName : interfaceNames) {
            InterfaceDefinition interfaceDefinition = InterfaceDefinition.builder()
                    .originalInterfaceName(interfaceName)
                    .customAlias(importedServices.getCustomAlias())
                    .integrationType(interfacesIntegrationType)
                    .applicationDomainColor(domainColor)
                    .linkToComponent(importedServices.getLinkToComponent())
                    .linkToCustomAlias(importedServices.getLinkToCustomAlias())
                    .build();
            // ignore call stack information
            log.debug("Extracted interface: {}", interfaceDefinition.getName());
            interfaceDefinitions.add(interfaceDefinition);
        }
        return interfaceDefinitions;
    }

    /**
     * Imports yaml and maps them to internal types
     *
     * @param path in filesystem from where the yamls are loaded
     * @return list of mapped services/applications
     */
    private List<ImportedServices> readYamlFilesFromFilesystem(final Path path) {
        List<ImportedServices> importedServicesList = new LinkedList<>();
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            ImportedServices importedServices = mapper.readValue(path.toFile(), ImportedServices.class);
            if (log.isDebugEnabled()) {
                log.debug(ReflectionToStringBuilder.toString(importedServices, ToStringStyle.MULTI_LINE_STYLE));
            }
            importedServicesList.add(importedServices);
        } catch (Exception e) {
            //Do nothing
            log.error("mapYamls exception: {}", e.getMessage());
        }
        return importedServicesList;
    }

    /**
     * Walks the target/ directory and searches for yaml-files containing
     * definitions of plantuml services
     * default: {user.dir}/target/
     */
    private List<Path> getFileList() {
        final String directory = StringUtils.defaultString(ImportAdapterImpl.GLOBAL_FILE_EXPORT_PATH, GLOBAL_FILE_EXPORT_PATH);

        try (Stream<Path> input = Files.walk(Paths.get(directory))) {
            return input
                    .filter(Objects::nonNull)
                    .filter(Files::isRegularFile)
                    .filter(YamlPredicate::isYamlFile)
                    .collect(Collectors.toList());
        } catch (IOException ioe) {
            log.error("Failed to find yaml files in {}", directory, ioe);
        }
        return Collections.emptyList();
    }

}
