package com.grolinger.java.controller.templatemodel;

public enum TemplateContent {
    START_DIAGRAM("@startuml\n"),
    END_DIAGRAM("@enduml"),
    DATE_PREFIX("' generated on "),
    CARRIAGE_RETURN("\n"),
    INCLUDE_PREFIX("!include "),
    SEQUENCE_HEADER_VARIABLES("'!$DETAILED=%true()\n" +
            "'!$SIMPLE=%true()\n" +
            "'!$SHOW_SQL=%false()\n" +
            "'!$SHOW_DOCUMENT_LINK=%false()\n" +
            "'!$SHOW_EXCEPTION=%true()\n\n"),
    COMPONENT_HEADER_VARIABLES("!$DETAILED=%true()\n" +
            "'!$UML_STRICT=%true()\n" +
            "!$SHOW_TODO=%true()\n\n\n");

    private final String content;

    TemplateContent(final String content) {
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }
}
