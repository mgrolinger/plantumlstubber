package com.grolinger.java.controller.templatemodel;

import lombok.AllArgsConstructor;
import lombok.Getter;

import static com.grolinger.java.controller.templatemodel.TemplateContent.COMPONENT_HEADER_VARIABLES;
import static com.grolinger.java.controller.templatemodel.TemplateContent.SEQUENCE_HEADER_VARIABLES;
import static com.grolinger.java.controller.templatemodel.Template.COMPONENT;
import static com.grolinger.java.controller.templatemodel.Template.SEQUENCE;

/**
 * Which plantUML diagram is processed,
 * which root directory is used for export
 * which template ist used to export
 * which header information are exported
 */
@Getter
@AllArgsConstructor
public enum DiagramType {
    COMPONENT_DIAGRAM_BASE("Component/", COMPONENT, COMPONENT_HEADER_VARIABLES),
    SEQUENCE_DIAGRAM_BASE("Sequence/", SEQUENCE,    SEQUENCE_HEADER_VARIABLES);

    private final String basePath;
    private final Template template;
    private final TemplateContent templateContent;

}
