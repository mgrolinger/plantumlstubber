package com.grolinger.java.controller;

import com.grolinger.java.controller.templatemodel.Template;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TemplateTest {

    @Test
    public void testGetContent() {

        assertThat(Template.COMPONENT.getTemplateURL())
                .isEqualTo("componentExport_V1_2020_7.html");
        assertThat(Template.SEQUENCE.getTemplateURL())
                .isEqualTo("sequenceExport_V1_2020_7.html");
    }
}