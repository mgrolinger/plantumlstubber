package com.grolinger.java.controller;

import com.grolinger.java.controller.templatemodel.TemplateContent;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TemplateContentTest {

    @Test
    public void testGetContent() {
        assertThat(TemplateContent.CARRIAGE_RETURN.getContent()).isEqualTo("\n");
    }
}