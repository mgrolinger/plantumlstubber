package com.grolinger.java.controller.templatemodel;

import org.junit.jupiter.api.Test;

import static com.grolinger.java.controller.templatemodel.DiagramType.*;
import static com.grolinger.java.controller.templatemodel.Template.COMPONENT;
import static com.grolinger.java.controller.templatemodel.Template.SEQUENCE;
import static org.assertj.core.api.Assertions.assertThat;

/**
 *
 */
public class DiagramTypeTest {

    @Test
    public void getBasePath() {
        assertThat(COMPONENT_DIAGRAM_BASE.getBasePath())
                .isEqualTo("Component/");
        assertThat(SEQUENCE_DIAGRAM_BASE.getBasePath())
                .isEqualTo("Sequence/");
    }

    @Test
    public void getTemplate() {
        assertThat(COMPONENT_DIAGRAM_BASE.getTemplate())
                .isEqualTo(COMPONENT);
        assertThat(SEQUENCE_DIAGRAM_BASE.getTemplate())
                .isEqualTo(SEQUENCE);
    }

    @Test
    public void getTemplateContent() {
        assertThat(COMPONENT_DIAGRAM_BASE.getTemplateContent())
                .isNotNull();
        assertThat(SEQUENCE_DIAGRAM_BASE.getTemplateContent())
                .isNotNull();
    }
}