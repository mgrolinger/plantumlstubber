package com.grolinger.java.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockitoAnnotations;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class NameConverterTest {

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @MethodSource
    private static Stream<Arguments> testGetReplaceUnwantedCharactersDataProvider() {
        String pathWithDots = "test.test";
        String restPath = "/api/resource/{id}/sub-resource";
        final boolean REPLACEDOTSONLY = true;
        final boolean REPLACEALL = false;
        return Stream.of(
                //@formatter:off
                Arguments.of(pathWithDots,     REPLACEDOTSONLY, "test_test"),
                Arguments.of(pathWithDots,     REPLACEALL,      "test_test" ),
                Arguments.of(pathWithDots+"/", REPLACEDOTSONLY, "test_test/"),
                Arguments.of(pathWithDots+"/", REPLACEALL,      "test_test_"),
                Arguments.of(pathWithDots+"_", REPLACEALL,      "test_test_"),
                Arguments.of("test-test1",     REPLACEALL,      "test_test1"),
                Arguments.of("test \n test1",   REPLACEALL,      "test_test1"),
                Arguments.of("test \\ test1",   REPLACEALL,      "test_test1"),
                Arguments.of(restPath,         REPLACEALL,      "_api_resource_id_sub_resource"),
                Arguments.of("/test-test/",    REPLACEALL,      "_test_test_"),
                Arguments.of(restPath,         REPLACEDOTSONLY, "/api/resource/id/sub_resource"),
                Arguments.of("",               REPLACEALL,      ""),
                Arguments.of(null,             REPLACEALL,      "")
                //@formatter:on
        );
    }

    @ParameterizedTest
    @MethodSource("testGetReplaceUnwantedCharactersDataProvider")
    public void testGetReplaceUnwantedCharacters(final String startValue, final boolean dots,
                                                 final String expectedValue) {
        String result = NameConverter.replaceUnwantedPlantUMLCharacters(startValue, dots);
        assertThat(result).isEqualTo(expectedValue);
    }

    @MethodSource
    private static Stream<Arguments> testReplaceUnwantedPlantUMLCharactersForPathDataProvider() {
        String pathWithDots = "test.test";
        String restPath = "/api/resource/{id}/sub-resource";
        return Stream.of(
                //@formatter:off
                Arguments.of(pathWithDots,      "test/test"),
                Arguments.of(pathWithDots+"/", "test/test/"),
                Arguments.of("test \n test",   "test/test"),
                Arguments.of("test \\ test",   "test/test"),
                Arguments.of("test//test",     "test/test"),
                Arguments.of(restPath,         "/api/resource/id/sub-resource"),
                Arguments.of("",               ""),
                Arguments.of(null,             "")
                //@formatter:on
        );
    }

    @ParameterizedTest
    @MethodSource("testReplaceUnwantedPlantUMLCharactersForPathDataProvider")
    public void testReplaceUnwantedPlantUMLCharactersForPath(final String startValue, final String expectedValue) {
        String result = NameConverter.replaceUnwantedPlantUMLCharactersForPath(startValue);
        assertThat(result).isEqualTo(expectedValue);
    }

}