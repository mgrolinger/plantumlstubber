package com.grolinger.java.service.data;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static com.grolinger.java.controller.templatemodel.Constants.DEFAULT_ROOT_SERVICE_NAME;
import static com.grolinger.java.controller.templatemodel.Constants.EMPTY;
import static org.assertj.core.api.Assertions.assertThat;

public class ServiceDefinitionTest {

    @MethodSource
    private static Stream<Arguments> serviceNameDataProvider() {
        final String simpleServiceName = "serviceName";
        final String restServiceName = "restServiceName/{subservice}/subs";
        final String soapServiceNameWithUnwantedChars = "soapServiceName.subservice.subs";
        final String defaultServiceName = DEFAULT_ROOT_SERVICE_NAME.getValue();
        return Stream.of(
                //@formatter:off
                Arguments.of(null,                            defaultServiceName ,                defaultServiceName),
                Arguments.of("",                              defaultServiceName,                 defaultServiceName),
                Arguments.of(EMPTY.getValue(),                           defaultServiceName,                 defaultServiceName),
                Arguments.of(simpleServiceName,                          simpleServiceName,                  simpleServiceName),
                Arguments.of(restServiceName,                 "restServiceName_subservice_subs" , restServiceName),
                Arguments.of("/v1/service-name/subs",         "v1_service_name_subs",             "/v1/service-name/subs"),
                Arguments.of(soapServiceNameWithUnwantedChars,"soapServiceName_subservice_subs",  soapServiceNameWithUnwantedChars)
                //@formatter:on
        );
    }

    @ParameterizedTest
    @MethodSource("serviceNameDataProvider")
    public void testServiceName(final String serviceName, final String expectedServiceCall, final String expectedName) {
        // given
        final String domainColorDefault = "domainColorDefault";
        ServiceDefinition cut = ServiceDefinition.builder()
                .serviceName(serviceName)
                .domainColor(domainColorDefault)
                .build();

        // when - then
        assertThat(cut.getServiceCallName()).isEqualTo(expectedServiceCall);
        assertThat(cut.getServiceLabel()).isEqualTo(expectedName);
        assertThat(cut.getDomainColor()).isEqualTo(domainColorDefault);
    }

    @MethodSource
    private static Stream<Arguments> servicePathDataProvider() {
        final String simpleServiceName = "serviceName";
        final String soapServiceNameWithUnwantedChars = "soapServiceName.subservice.subs";
        final String defaultServiceName = DEFAULT_ROOT_SERVICE_NAME.getValue();
        return Stream.of(
                //@formatter:off
                Arguments.of(null,                                 defaultServiceName,                  ""),
                Arguments.of("",                                   defaultServiceName,                  ""),
                Arguments.of("     ",                              defaultServiceName,                  ""),
                Arguments.of(EMPTY.getValue(),                     defaultServiceName,                  ""),
                Arguments.of(simpleServiceName,                    simpleServiceName+"/",                      "../"),
                Arguments.of("restServiceName/{subservice}/subs",  "restServiceName/subservice/subs/",  "../../../" ),
                Arguments.of("/restServiceName/{subservice}/subs", "restServiceName/subservice/subs/",  "../../../" ),
                Arguments.of("/v1/service-name/subs",              "v1/service-name/subs/",             "../../../" ),
                Arguments.of(soapServiceNameWithUnwantedChars,     "soapServiceName/subservice/subs/",  "../../../")
                //@formatter:on
        );
    }

    @ParameterizedTest
    @MethodSource("servicePathDataProvider")
    public void testservicePath(final String serviceName, final String expServicePath, final String expCommonPath) {
        // given
        ServiceDefinition cut = ServiceDefinition.builder()
                .serviceName(serviceName)
                .build();

        // when - then
        assertThat(cut.getPath()).isEqualTo(expServicePath);
        assertThat(cut.getPathToRoot()).isEqualTo(expCommonPath);
    }
}