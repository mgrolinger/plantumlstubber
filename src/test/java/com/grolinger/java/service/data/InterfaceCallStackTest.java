package com.grolinger.java.service.data;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class InterfaceCallStackTest {
    private InterfaceCallStack cut;

    @MethodSource
    private static Stream<Arguments> testBooleanCallStackProvider() {
        return Stream.of(
                //@formatter:off
                Arguments.of("",                                    false ),
                Arguments.of("/api/interface",                      false ),
                Arguments.of("/api/interface<<DOMAIN>>::GET:POST",  false ),
                Arguments.of("/api/interface<<DOMAIN>>->Call_stack->Call_stack2::GET:POST", true ),
                Arguments.of("/api/interface::GET:POST<<DOMAIN>>->Call_stack->Call_stack2", true ),
                Arguments.of("/api/interface::GET:POST->Call_stack->Call_stack2<<DOMAIN>>", true )
                //@formatter:on
        );
    }

    @ParameterizedTest
    @MethodSource("testBooleanCallStackProvider")
    public void testContains(final String originalInterface, final boolean expected) {
        cut = new InterfaceCallStack(originalInterface);
        assertThat(cut.containsCallStack()).isEqualTo(expected);
    }

    @MethodSource
    private static Stream<Arguments> testInterfaceNameProvider() {
        return Stream.of(
                //@formatter:off
                Arguments.of("interface",                                                   "interface" ),
                Arguments.of("/api/interface",                                              "/api/interface" ),
                Arguments.of("/api/interface<<DOMAIN>>::GET:POST",                          "/api/interface" ),
                Arguments.of("/api/interface<<DOMAIN>>->Call_stack->Call_stack2::GET:POST", "/api/interface" ),
                Arguments.of("/api/interface::GET:POST<<DOMAIN>>->Call_stack->Call_stack2", "/api/interface" ),
                Arguments.of("/api/interface::GET:POST->Call_stack->Call_stack2<<DOMAIN>>", "/api/interface" ),
                Arguments.of("/api/interface->Call_stack->Call_stack2::GET:POST<<DOMAIN>>", "/api/interface" ),
                Arguments.of("/api/interface->Call_stack->Call_stack2<<DOMAIN>>::GET:POST", "/api/interface" )
                //@formatter:on
        );
    }

    @ParameterizedTest
    @MethodSource("testInterfaceNameProvider")
    public void testInterfaceName(final String originalInterface, final String expectedInterfaceName) {
        cut = new InterfaceCallStack(originalInterface);
        assertThat(cut.getInterfaceName()).isEqualTo(expectedInterfaceName);
    }

    @MethodSource
    private static Stream<Arguments> testCallStackProvider() {
        String[] methods = new String[]{"Call_stack", "Call_stack2"};
        String[] incl = new String[]{"Call/stack", "Call/stack2"};
        return Stream.of(
                //@formatter:off
                Arguments.of("/api/interface",                                                  null, null ),
                Arguments.of("/api/interface<<DOMAIN>>::GET:POST",                              null, null ),
                Arguments.of("/api/interface<<DOMAIN>>->Call_stack->Call_stack2::GET:POST",     methods, incl ),
                Arguments.of("/api/interface::GET:POST<<DOMAIN>>->Call_stack->Call_stack2",     methods, incl ),
                Arguments.of("/api/interface_new::GET:POST->Call_stack->Call_stack2<<DOMAIN>>", methods, incl ),
                Arguments.of("/api/interface_new::GET:POST->Call_stack->Call_stack2<<DOMAIN>>", methods, incl )
                //@formatter:on
        );
    }

    @ParameterizedTest
    @MethodSource("testCallStackProvider")
    public void testCallStack(final String originalInterface, final String[] expectedMethod, final String[] expectedIncludes) {
        cut = new InterfaceCallStack(originalInterface);
        assertThat(cut.getCallStackMethods()).isEqualTo(expectedMethod);
        assertThat(cut.getCallStackIncludes()).isEqualTo(expectedIncludes);
    }
}