package com.grolinger.java.service.data;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class MethodDefinitionTest {
    @MethodSource
    private static Stream<Arguments> testDomainColorProvider() {

        return Stream.of(
                //@formatter:off
            Arguments.of(null,                              Collections.emptyList() ),
            Arguments.of("",                                Collections.emptyList() ),
            Arguments.of("/api/interface",                  Collections.emptyList() ),
            Arguments.of("/api/interface<<DOMAIN>>",        Collections.emptyList() ),
            // wrong MethodSeparator : instead of ::
            Arguments.of("/api/interface:GET",              Collections.emptyList() ),
            // definition only with Method
            Arguments.of("/api/interface<<DOMAIN>>::GET",   Collections.singletonList(HttpMethod.GET)),
            // full definition
            Arguments.of("/api/interface<<DOMAIN>>::GET:POST->Call_stack->Call_stack2", Arrays.asList(HttpMethod.GET,HttpMethod.POST) ),
            Arguments.of("/api/interface::GET:POST<<DOMAIN>>->Call_stack->Call_stack2", Arrays.asList(HttpMethod.GET,HttpMethod.POST)),
            Arguments.of("/api/interface<<DOMAIN>>->Call_stack->Call_stack2::GET:POST:PATCH:DELETE", List.of(HttpMethod.GET,
                                                                                        HttpMethod.POST,HttpMethod.PATCH,
                                                                                        HttpMethod.DELETE) )
        );
            //@formatter:on
    }

    @ParameterizedTest
    @MethodSource("testDomainColorProvider")
    public void testGetDomainColor(final String originalInterfaceName, final List<String> expResult) {
        MethodDefinition methodDefinition = new MethodDefinition(originalInterfaceName);
        assertThat(methodDefinition.getMethods()).isEqualTo(expResult);
    }
}
