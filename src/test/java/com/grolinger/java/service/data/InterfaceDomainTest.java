package com.grolinger.java.service.data;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class InterfaceDomainTest {

    @MethodSource
    private static Stream<Arguments> testDomainColorProvider() {

        return Stream.of(
                //@formatter:off
                Arguments.of(null,   null,  "default" ),
                Arguments.of(null,          "app", "app" ),
                Arguments.of("",             null,  "default"),
                Arguments.of("",            "app", "app" ),
                Arguments.of("domain",      "app", "app"),
                Arguments.of("<<doMaIn>>",  "app", "domain" ),
                Arguments.of("<<DOMAIN>>",  "app", "domain" ),
                // definition with domainColor
                Arguments.of("/api/interface<<DOMAIN>>",        "app", "domain" ),
                // definition only with Method
                Arguments.of("/api/interface<<DOMAIN>>::GET",   "app", "domain" ),
                // full definition
                Arguments.of("/api/interface<<DOMAIN>>::GET:POST->Call_stack->Call_stack2",  "app", "domain" ),
                Arguments.of("/api/interface::GET:POST<<DOMAIN>>->Call_stack->Call_stack2",  "app", "domain" ),
                Arguments.of("/api/interface::GET:POST->Call_stack->Call_stack2<<DOMAIN>>",  "app", "domain" )
                //@formatter:on
        );
    }

    @ParameterizedTest
    @MethodSource("testDomainColorProvider")
    public void testGetDomainColor(final String originalInterfaceName, final String color, final String expResult) {
        assertThat(InterfaceDomain.extractDomainColor(originalInterfaceName, color)).isEqualTo(expResult);
    }
}