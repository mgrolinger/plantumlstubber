package com.grolinger.java.service.data;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class InterfaceDefinitionTest {


    @MethodSource
    private static Stream<Arguments> testNameDataProvider() {
        InterfaceDefinition one = InterfaceDefinition.builder()
                .originalInterfaceName("method")
                .integrationType("SoAp")
                .build();
        List<String> resultOne = List.of("method");
        InterfaceDefinition two = InterfaceDefinition.builder()
                .originalInterfaceName("/api/v1/resource/{id}::POST:GET->Call_Some_Application->And_some_other")
                .integrationType("rEsT::XML")
                .build();
        List<String> resultTwo = List.of("api", "v1", "resource", "id");
        InterfaceDefinition three = InterfaceDefinition.builder()
                .originalInterfaceName("/api/v1/resource/{id}/")
                .integrationType("rEsT")
                .build();
        List<String> resultThree = List.of("api", "v1", "resource", "id");
        // Oh well this will later cause an exception while exporting the file,
        // because we won't have a name for the file
        InterfaceDefinition four = InterfaceDefinition.builder()
                .originalInterfaceName("/")
                .integrationType("rEsT")
                .build();
        List<String> resultFour = Collections.emptyList();
        InterfaceDefinition five = InterfaceDefinition.builder()
                .originalInterfaceName("/")
                .integrationType("foo::bar")
                .build();
        List<String> resultFive = Collections.emptyList();
        InterfaceDefinition six = InterfaceDefinition.builder()
                .originalInterfaceName("/withCallStack/{id}/foo->Filesystem_readFile->Filesystem_writeFile<<authentifizierung>>::POST:GET")
                .applicationDomainColor("authentifizierung")
                .integrationType("rest")
                .build();
        InterfaceDefinition sixA = InterfaceDefinition.builder()
                .originalInterfaceName("/withCallStack/{id}/foo::POST:GET->Filesystem_readFile->Filesystem_writeFile<<authentifizierung>>")
                .applicationDomainColor("authentifizierung")
                .integrationType("rest")
                .build();
        List<String> resultSix = List.of("withCallStack", "id", "foo");
        return Stream.of(
                //@formatter:off
                Arguments.of(one   , "method",                 resultOne,   "method",               "SOAP::XML" , "method", false),
                Arguments.of(two   , "/api/v1/resource/{id}",  resultTwo,   "api_v1_resource_id",   "REST::XML" , "id"  , true),
                // The slash prevents a method name
                Arguments.of(three , "/api/v1/resource/{id}/", resultThree, "api_v1_resource_id",   "REST::JSON",  "" , false ),
                Arguments.of(four  , "/",                      resultFour,  ""                  ,   "REST::JSON",  "" , false ),
                Arguments.of(five  , "/",                      resultFive,  ""                  ,   "FOO::BAR",    "" , false ),
                Arguments.of(six,  "/withCallStack/{id}/foo",  resultSix,   "withCallStack_id_foo", "REST::JSON",  "foo",true ),
                Arguments.of(sixA,  "/withCallStack/{id}/foo",  resultSix,   "withCallStack_id_foo", "REST::JSON",  "foo",true)
                //@formatter:on
        );
    }

    @ParameterizedTest
    @MethodSource("testNameDataProvider")
    public void testName(final InterfaceDefinition cut,
                         final String expName,
                         final List<String> expParts,
                         final String expCallName,
                         final String expInteg,
                         final String expMethod,
                         final boolean hasCallStack) {
        assertThat(cut.getName()).isEqualTo(expName);
        assertThat(cut.getNameParts()).isEqualTo(expParts);
        assertThat(cut.getCallName()).isEqualTo(expCallName);
        assertThat(cut.getIntegrationType()).isEqualTo(expInteg);
        assertThat(cut.getMethodName()).isEqualTo(expMethod);
        if (hasCallStack) {
            assertThat(cut.getCallStack().getCallStackMethods()).isNotEmpty();
            assertThat(cut.getCallStack().getCallStackIncludes()).isNotEmpty();
        } else {
            assertThat(cut.getCallStack().getCallStackMethods()).isNull();
        }
    }

    @MethodSource
    private static Stream<Arguments> testContainsPathDataProvider() {
        //This case should not happen
        InterfaceDefinition zero = InterfaceDefinition.builder()
                .originalInterfaceName("")
                .integrationType("SoAp")
                .build();
        InterfaceDefinition one = InterfaceDefinition.builder()
                .originalInterfaceName("method")
                .integrationType("SoAp")
                .build();
        InterfaceDefinition two = InterfaceDefinition.builder()
                .originalInterfaceName("/api/v1/resource/{id}::POST:GET->Call_Some_Application->And_some_other")
                .integrationType("rEsT")
                .build();
        InterfaceDefinition three = InterfaceDefinition.builder()
                .originalInterfaceName("/api/v1/resource/{id}/")
                .integrationType("rEsT")
                .build();
        return Stream.of(
                //@formatter:off
                Arguments.of(zero,  false,  false),
                Arguments.of(one,   true,   false),
                Arguments.of(two,   true,   true),
                Arguments.of(three, true,   true)
                //@formatter:on
        );
    }

    @ParameterizedTest
    @MethodSource("testContainsPathDataProvider")
    public void testContainsPath(final InterfaceDefinition cut, final boolean expContainsPath, final boolean expSlash) {
        assertThat(cut.containsPath()).isEqualTo(expContainsPath);
        assertThat(cut.getPath().contains("/")).isEqualTo(expSlash);
    }

    @MethodSource
    private static Stream<Arguments> testContainsCallStackDataProvider() {
        InterfaceDefinition one = InterfaceDefinition.builder()
                .originalInterfaceName("method")
                .integrationType("SoAp")
                .build();
        InterfaceDefinition two = InterfaceDefinition.builder()
                .originalInterfaceName("/api/v1/resource/{id}<<domainColor>>::POST:GET->Call_Some_Application->And_some_other")
                .integrationType("rEsT")
                .build();

        return Stream.of(
                //@formatter:off
                Arguments.of(one, false, null,                                                      null),
                Arguments.of(two, true,  new String[]{"Call_Some_Application", "And_some_other"},   new String[]{"Call/Some/Application", "And/some/other"})
                //@formatter:on
        );
    }

    @ParameterizedTest
    @MethodSource("testContainsCallStackDataProvider")
    public void testContainsCallStack(final InterfaceDefinition cut,
                                      final boolean containsCallStack,
                                      final String[] callStack,
                                      final String[] includesStack) {
        assertThat(cut.containsCallStack()).isEqualTo(containsCallStack);
        assertThat(cut.getCallStack().getCallStackMethods()).isEqualTo(callStack);
        assertThat(cut.getCallStack().getCallStackIncludes()).isEqualTo(includesStack);
    }

    @Test
    public void testBuilder() {
        InterfaceDefinition.InterfaceDefinitionBuilder result = InterfaceDefinition.builder();
        assertThat(result).isNotNull();
    }

    @MethodSource
    private static Stream<Arguments> testGetPathToRootDataProvider() {
        InterfaceDefinition one = InterfaceDefinition.builder()
                .originalInterfaceName("/api/v1/resource/{id}::POST:GET->Call_Some_Application")
                .integrationType("rEsT")
                .build();
        InterfaceDefinition two = InterfaceDefinition.builder()
                .originalInterfaceName("method")
                .integrationType("SoAp")
                .build();

        return Stream.of(
                //@formatter:off
                Arguments.of(one,"../../.."),
                Arguments.of(two ,"")
                //@formatter:on
        );
    }

    @ParameterizedTest
    @MethodSource("testGetPathToRootDataProvider")
    public void testGetPathToRoot(final InterfaceDefinition cut, final String expRootPath) {
        assertThat(cut.getPathToRoot()).isEqualTo(expRootPath);

    }

    @MethodSource
    private static Stream<Arguments> testGetMethodDataProvider() {
        InterfaceDefinition one = InterfaceDefinition.builder()
                .originalInterfaceName("/api/v1/resource/{id}::POST:GET->Call_Some_Application")
                .integrationType("rEsT")
                .build();
        List<HttpMethod> expResultOne = Arrays.asList(HttpMethod.POST, HttpMethod.GET);

        InterfaceDefinition two = InterfaceDefinition.builder()
                .originalInterfaceName("method")
                .integrationType("SoAp")
                .build();
        List<HttpMethod> expResultTwo = Collections.emptyList();

        InterfaceDefinition three = InterfaceDefinition.builder()
                .originalInterfaceName("method::")
                .integrationType("SoAp")
                .build();
        List<HttpMethod> expResultThree = Collections.emptyList();

        InterfaceDefinition four = InterfaceDefinition.builder()
                .originalInterfaceName("method")
                .integrationType("SomethingUnknown")
                .build();

        return Stream.of(
                //@formatter:off
                Arguments.of(one,   expResultOne),
                Arguments.of(two ,  expResultTwo),
                Arguments.of(three, expResultThree),
                Arguments.of(four, expResultThree)
                //@formatter:on
        );
    }

    @ParameterizedTest
    @MethodSource("testGetMethodDataProvider")
    public void testGetMethodDefinition(final InterfaceDefinition cut, final List<HttpMethod> expMethods) {
        assertThat(cut.getMethodDefinition()).isNotNull();
        assertThat(cut.getMethodDefinition().getMethods()).isEqualTo(expMethods);
    }
}